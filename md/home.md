# flomusic.net

You have **human tech rights**. These are defined by the [Electronic Frontier Alliance Principles](https://www.eff.org/fight)

1. Free Expression
People should be able to speak their minds to whoever will listen.
 
2. Security
Technology should be trustworthy and answer to its users.
 
3. Privacy
Technology should allow private and anonymous speech, and allow users to set their own parameters about what to share with whom.
 
4. Creativity
Technology should promote progress by allowing people to build on the ideas, creations, and inventions of others.
 
5. Access to Knowledge
Curiosity should be rewarded, not stifled.
We uphold these principles by fighting for transparency and freedom in culture, code, and law. 

You have **human music rights**. These are defined here:

1. Right to listen

2. Right to study

3. Right to perform

4. Right to adapt

5. Right to compose

**FLO** stands for **Free/Libre/Open**. This is defined at [snowdrift.coop](snowdrift.coop) [here](https://wiki.snowdrift.coop/about/free-libre-open#flo).

J.S. Bach (1685-1750) copied scores by hand. Bach also reharmonized popular melodies of his time. He was free to use the tuning technology of his time however he wished. He taught his students the mysteries of music without fear of getting a DMCA takedown notice.

Learning by ear is a tradition that began long before the technology of recording music. For many musicians, this is the primary way to learn music. "Covering a song," which means performing a recorded song, is an essential part to both learning how to become a professional, as well as an essential part to being a professional. In styles such as jazz, adapting a popular song with different compositional choices, just as J.S. Bach and countless others have done for centuries, is an integral part to the genre itself.

These past freedoms of music are challenged in the world of today. The recording industry, which is monopolized by 3 companies, gives out bogus copyright claims generated by A.I. YouTube, part of the Google monopoly, ignores fair use and completely disregards content creators rights in favor of the recording industry. You can read them [here](https://www.youtube.com/howyoutubeworks/policies/copyright/#fair-use) avoiding the problem and completely ignoring musicians. It makes "fair use" sound less like a right and more like a privilege. Their suggested solution of their "Audio Library" completely ignores musicians who compose, teach, or perform music.

What are our rights? What should be our rights?

You can review your rights above. flomusic.net exists to advance those rights.
