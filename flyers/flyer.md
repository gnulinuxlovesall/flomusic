# FLO Music: A new way of music

## What is FLO?
  
Free / Libre / Open

## What is FLO Music?

A method of combining music, education, and technology in a manner free from restrictions.

Visit <https://flomusic.net>
